/*
 * This script: 
 *   - need an array of one2many elements  like order lines, order events
 *  
 *   - add tbody with classname is like [name of element] with the  link having class like "add_[name of element]_link"
 *   - the tbody lines must have a delete link with class like "remove_[name of element]_link"
 *   
 *   ex: 
 *   add:       class="add_docline_link btn btn-primary"
 *   remove :    class="remove_docline_link btn btn-primary"
 *   tbody:       class="docline" 
 *   
 */
jQuery(document).ready(function() {
    
	  	  
	var removeDocLineForm  = function (){
        // remove the li for the tag form
        $(this).parents(':eq(1)').remove();
  
}

	
      for (var i = 0; i < $els.length; i++) {
    	  
    	 
    	  
    	  
              $('a.add_'+$els[i]+'_link').on('click', function(e) {
              // prevent the link from creating a "#" on the URL
              e.preventDefault();
              // add a new tag form (see next code block)
              var arrayId = $(this).attr('class').split('_');
              addForm($('tbody.'+arrayId[1]));    });        
            removeline(  $('span.remove_'+$els[i]+'_link')) ;
      }
    
    function addForm(collectionHolder) {
            // Get the data-prototype explained earlier
            var prototype = collectionHolder.data('prototype');
            // get the new index
       //     var index = $collectionHolder.data('index',$collectionHolder.find('tr').length);
            var index = collectionHolder.find('tr').length;
            // Replace '__name__' in the prototype's HTML to
            // instead be a number based on how many items we have
            var newForm = prototype.replace(/__name__/g, index);
            // increase the index with one for the next item
            collectionHolder.data('index', index + 1);
            // Display the form in the page in an li, before the "Add a tag" link li
            collectionHolder.append(newForm);
              var arrayId = collectionHolder.attr('class').split('.');
            removeline( $('span.remove_'+arrayId[0]+'_link'));
      }

    
function removeline( removeDocLineLink) {
            for (var i = 0; i < $(removeDocLineLink).length; i++) {
                 
            	$(removeDocLineLink[i]).click(removeDocLineForm);
         
     }
}

});