<?php

namespace DLMAPP\ContactsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name') 
		->add('firstname') 
		->add('address') 
		->add('address2') 
		->add('postalCode') 
		->add('town') 
		->add('phone') 
		->add('phone2') 
		->add('fax') 
		->add('email') 
		->add('url') 
		->add('imageFile',  VichImageType::class, [
		    'required' => false,
		    'allow_delete' => true,
		    'download_label' => '...',
		    'download_uri' => true,
		    'image_uri' => true,
		    'imagine_pattern' => '',
		]) 
		->add('note') 
		;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DLMAPP\ContactsBundle\Entity\Contact'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dlmapp_contactsbundle_contact';
    }


}
