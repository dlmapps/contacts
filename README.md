# Contacts
## Pourquoi?
Cette application de gestion de contacts a pour objectif de partager mon savoir faire. Elle fait suite aux demandes de recruteurs. A l'origine, développeuse Backend, j'étends mes compétences et manipulant inkscape, afin d'illustrer l'application. 

## Comment?
Dans mon métier, j'aime être à l'écoute des utilisateurs, et développer rapidement les fonctionnalités dont ils auront besoin. 
Pour cela, je m'appuie sur des outils proposant des fonctionnalités de base et plus encore pour m'affranchir des développements bas niveau.
Pour le visuel et le responsive, je m'appuie sur l'incontournable [Bootstrap 3.3](http://getbootstrap.com/docs/3.3). 
Pour le socle des développements, je m'appuie sur [Symfony 3.3.8](https://symfony.com/doc/current/index.html) avec son moteur de template twig et son composant ORM Doctrine.
Symfony, par son bundle  [SensioGeneratorBundle](https://github.com/sensio/SensioGeneratorBundle), génère les fonctionnalités CRUD ( la possibilité de gérer rapidement les données de la base de données). 
L'usage régulier de ce bundle m'a incité à modifier son comportement pour répondre à mes attentes, cela donne le bundle [crudgeneratorbundle](https://packagist.org/packages/dlmappstools/crudgeneratorbundle).

### Focus dlmappstools/crudgeneratorbundle

Ce bundle fonctionne comme le SensioGeneratorBundle. A partir de l'objet contact, la commande Symfony "php bin/console dlmappstools:doctrine:generate:crud" développe les fonctionnalités CRUD suivantes: liste des contacts, la possibilité d'ajouter/modifier un contacts et de supprimer un contact.

Les vues sont générées, à partir d'un squelette, dans le répertoire resources/views du bundle concerné par la commande. La commande génère par défaut les actions d'écriture. Les routes sont au format yml par défaut. Le fait d'overwriter l'existant ou non devient une question.
Parmi les évolutions prévues, revoir le squelette des vues pour permettre l'internationalisation, incorporer un formulaire pour les vues New/Edit.

## fonctionnalités de l'application contact?

#### actives

| fonctionnalités |
| ------ | 
| Lister les contacts  |
| Ajouter un contact |
| Modifier un contact  | 
| supprimer un contact | 
| Ajouter un évènement à un contact | 
| Supprimer un évènement à un contact | 

#### prévues

| Fonctionnalités |
| ------ | 
| Pages d'erreur personnalisées |
| Internationalisation |

