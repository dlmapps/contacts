<?php

namespace DLMAPP\ContactsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DLMAPPContactsBundle:Default:index.html.twig');
    }
}
